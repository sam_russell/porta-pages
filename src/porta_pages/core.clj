(ns porta-pages.core
  ;;(:require [porta-owl.core :as owl-sqwrl])
  (:require [porta-owl.owl :as owl])
  (:require [orgmode-parser.core :as org])
  (:use [porta-owl.core])
  (:use [hiccup.core])
  (:gen-class))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(def img-exts
  ["png"
   "jpeg"
   "jpg"
   "gif"
   "tiff"
   "tif"
   "xbm"
   "xpm"
   "pbm"
   "pgm"
   "ppm"
   "pnm"
   "svg"])


(def org-file
  (org/parse-str (slurp "resources/test.org")))

(defn ignore-comments [content-list]
  (filter
   (fn [i] (if (map? i)
             (if (:type i)
             true))
   content-list)))


(defn section-header [s]
  (let [level (keyword (clojure.string/join ["h" (str (s :level))]))]
    (html [level (s :text)])))


;; images will need to be contained within a header
(defn image-buh
  ([i] (html [:img {:src (i :uri)}])))


  ;; random idea for SWRL rules:  a TODO item that is older than a week
  ;; is a hard problem.
  ;; a TODO item that is older than two weeks is a very hard problem :)

(defn get-body [page]
  (:content (first (:content page))))


(defontology
  "http://sammysvirtualzoo.com/ontology#"
  'homepage)
(with-ontology homepage
  (defentity asset [::owl/Class "asset"]))
(def rules (rule-engine homepage/ontology))
(def sqwrl (sqwrl-engine homepage/ontology))
(def test-img (second (:content (second (get-body org-file)))))
(def img-edn (clojure.string/join "" (:content (first (test-img :content)))))

(defn derive-owl-namespace []
  (porta-owl.owl/derive-axiom-dispatch)
  (porta-owl.owl/derive-collection-axiom-dispatch)
  (porta-owl.owl/derive-collection-exp-dispatch)
  (porta-owl.owl/derive-entity-types)
  (porta-owl.owl/derive-facets)
  (porta-owl.owl/derive-restriction-dispatch))

(defn owl-ns [edn-str]
  (clojure.string/replace edn-str "::owl" ":porta-owl.owl"))

(defn read-owl-data [edn-block]
  (clojure.edn/read-string (owl-ns edn-block)))

(defrecord Rule [onto rule-engine rule])

(defrecord Query [onto sqwrl-engine query])

;;;(defn image [img-data rule-map]

;;(defn paragraph [p]
;;  (html 


                         
